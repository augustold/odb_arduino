#include <string>

const char* odb_logo =
	"MMMMMMMMMMMNhs-.``    ``.-shNMMMMMMMMMMM\n"
	"MMMMMMMMdo-                  -odMMMMMMMM\n"
	"MMMMMMy-    -oymNMMMMMMNmyo-    -yMMMMMM\n"
	"MMMMh.   -yNMMMMMMMMMMMMMMMMNy-   .hMMMM\n"
	"MMN/   -dMMMMMMMMMNmhs+:/MMMMMMd-   /NMM\n"
	"MN-   sMMMMMdyo-`       -MMMMMMMMs   -NM\n"
	"M:   hMMMMMm            -MMMMMMMMMh   :M\n"
	"y   oMMMMMMMMMM+        :MMMMMMMMMMo   y\n"
	"-  `NMMMMMMMMMM+        :MMMMMMMMMMN`  -\n"
	".  :MMMMMMMMMMM+        :MMMMMMMMMMM:  .\n"
	".  :MMMMMMMMMMM+        :MMMMMMMMMMM:  .\n"
	"-  `NMMMMMMMMMM/        :MMMMMMMMMMN`  -\n"
	"y   oMMMMMMMMMM/        -MMMMMMMMMMo   y\n"
	"M:   hMMMMMMMMM/        -MMMMMMMMMh   :M\n"
	"MN-   sMMMMMMMM/        -MMMMMMMMs   -NM\n"
	"MMN/   -dMMMMMM/;;;;;;;;:MMMMMMd-   /NMM\n"
	"MMMMh.   -yNMMMMMMMMMMMMMMMMNy-   .hMMMM\n"
	"MMMMMMy-    -oymNMMMMMMNmyo-    -yMMMMMM\n"
	"MMMMMMMMdo-                  -odMMMMMMMM\n"
	"MMMMMMMMMMMNhs-.``    ``.-shNMMMMMMMMMMM";
