#include <iostream>
#include <string>
#include <cstdlib>
#include "one_dollar_board_ascii.h"

#ifdef _WIN32
#define OUTPUT_NULL " > nul"
#else
#define OUTPUT_NULL " > /dev/null"
#endif

int main(int argc, char* argv[])
{
	std::string original = argv[0];
	std::string argument = argv[1];

	size_t found;
	found = original.find_last_of("/\\");
	std::string uploader = "\""+original.substr(0,found+1)+"micronucleus\" --timeout 60 --no-ansi --run " + argument;

	std::cout << "Please, plug in or restart your device now... (will timeout in 60 seconds)" << std::endl;
	int micronucleus_return = system(uploader.append(OUTPUT_NULL).c_str());
	if (micronucleus_return == EXIT_SUCCESS) {
		std::cout << std::endl << odb_logo << std::endl;
		std::cout << "Your One Dollar Board is ready. Thank you!" << std::endl;
		return EXIT_SUCCESS;
	} else {
		std::cout << "\n\n\n\n";
		std::cout << "Program upload failed. Please, unplug your One Dollar Board and try again" << std::endl;
		return EXIT_FAILURE;
	}
}
