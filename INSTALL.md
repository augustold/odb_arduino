# One Dollar Board

## Requisitos
* Arduino IDE 1.5.x ou maior

## Instalação
* Na IDE, abra Arquivo -> Preferências;
* No campo "URLS Adicionais de Gerenciadores de Placas:" adicione o seguinte link e clique em OK:

	https://bitbucket.org/augustold/odb_arduino/raw/fdd23e1a85ed76c3e99f4d213e0030c01b091da5/package_onedollarboard_index.json

* Agora abra Ferramentas -> Placa:["Nome de placa"] -> Gerenciador de Placas...
* No campo "Tipos", selecione a opção "Contribuiu"
* Selecione a placa One Dollar Board e clique em "Instalar"

### GNU/Linux
É necessário dar permissão de execução para o programador. Para isso, basta baixar, descompactar e executar o
script contido no arquivo.

	https://bitbucket.org/augustold/odb_arduino/raw/8230a0b132543862849345b010949379b0556e8e/micronucleus-2.0a5-linux64/micronucleus-2.0a5-linux-permissions.zip


## Como usar
* Selecionar a placa One Dollar Board em Ferramentas -> Placa:["Nome de placa"]
* Após finalizar o código e clicar no botão Carregar da IDE, é necessário reiniciar a placa One Dollar Board, seja através do botão de reset
ou da reintrodução da mesma na porta USB.
